# HookTuber

CLI tool written in C# to convert youtube links to hooktube links and open them in your default browser.

## How to use
Either add the path to the compiled exe to your environment variables in powershell and then invoke it from the CLI, or just launche the executable and use the console that opens.

Then just paste the youtube link and then press enter.

[Link to exe](https://1drv.ms/u/s!AlZZhB75siHNhKI3BCJAxsuxyr-o2g)