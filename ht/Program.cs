﻿using System;
using System.Diagnostics;

namespace ht
{
    class Program
    {
        static void Main(string[] args)
        {
            string link;
            string input = "";
            if (args.Length > 0 && args[0].Contains("youtube"))
            {
                link = args[0].Replace("youtube", "hooktube");
                Process.Start(link);
            }
            else if (args.Length > 0 && args[0].Contains("youtu.be"))
            {
                link = args[0].Replace("youtu", "hooktube").Replace(".be/", ".com/watch?v=");
                Process.Start(link);
            }
            Console.WriteLine("Paste link of youtube video or \"exit\" to quit...");
            while (input != "exit")
            {
                input = Console.ReadLine();
                if (input.Contains("youtube"))
                {
                    link = input.Replace("youtube", "hooktube");
                    Process.Start(link);
                }
                else if (input.Contains("youtu.be"))
                {
                    link = input.Replace("youtu", "hooktube").Replace(".be/", ".com/watch?v=");
                    Process.Start(link);
                }
                else
                {
                    Console.WriteLine("Wrong input, must be a youtube link...");
                }
            }
        }
    }
}